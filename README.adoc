= docker-asciidoctor-extended
:toc:
:source-highlighter: highlightjs
:source-language: bash
:url-adocbibtex: https://github.com/asciidoctor/asciidoctor-bibtex[asciidoctor-bibtex]
:url-adocdiagram: https://asciidoctor.org/docs/asciidoctor-diagram/[asciidoctor-diagram]
:url-adocdocker: https://github.com/asciidoctor/docker-asciidoctor[docker-asciidoctor]
:url-adocug: https://asciidoctor.org/docs/user-manual/[Asciidoctor user manual]
:url-bibtex: http://www.bibtex.org/[BibTeX]
:url-citeseerx: https://citeseerx.ist.psu.edu/CiteSeerX[CiteSeerX]
:url-graphviz: https://graphviz.org/[Graphviz]
:url-jabref: https://www.jabref.org/[Jabref]
:url-mendeley: https://www.mendeley.com/[Mendeley]
:url-plantuml: https://plantuml.com/[PlantUML]
:url-vega: https://vega.github.io/vega/[Vega]
:url-vegalite: https://vega.github.io/vega-lite/[Vega-Lite]
:url-zotero: https://www.zotero.org/[Zotero]

This Docker image adds several extensions to the official {url-adocdocker}
image.

== Additional Functionality

=== Diagrams: Vega and Vega-Lite

{url-adocdiagram} is included in the official Asciidoctor Docker image.  At the
time of writing, it supports numerous diagram types, but only ships the
dependencies to use {url-graphviz} and {url-plantuml}.  However, {url-vega} and
{url-vegalite} are the only diagram types which can draw “general purpose”
charts such as bar charts, line charts, scatter plots, etc.  The Vega project
is fully featured and actively being developed, so investing in it is
worthwhile.

=== BibTeX

Asciidoctor provides rudimentary bibliography management.  But if one uses
another tool for bibliography management, such as {url-jabref}, {url-mendeley},
{url-zotero}, or even just download references from Google Books or
{url-citeseerx}, then it is convenient to use {url-bibtex}.  The
{url-adocbibtex} plug-in is not included with docker-asciidoctor, so it is
included in this Docker image.

== Using the Docker Image

To build the Docker image:

[source]
----
docker build -t asciidoctor-extended .
----

To launch base on the image:

[source]
----
docker run -it -v /path/on/local/machine:/documents/ asciidoctor-extended
----

To compile a document directly:

[source]
----
docker run -it -v /path/on/local/machine:/documents/ asciidoctor-extended asciidoctor input.adoc -o output.html
----

Here `/path/on/local/machine` is a directory on your computer which will be
mounted in Docker, so that those files are accessible within the Docker image.

Turn on Asciidoctor extensions with `-r`, for example

[source]
----
docker run -it -v /path/on/local/machine:/documents/ asciidoctor-extended asciidoctor -r asciidoctor-diagram -r asciidoctor-bibtex input.adoc -o output.html
----

For more information, see the documentation for {url-adocdocker}, and the
{url-adocug}.

=== Examples

A couple of examples are included to showcase the features of the
Asciidoctor-BibTeX-Vegalite combination.  They were compiled with the following
commands, respectively:

[source, linenums]
----
asciidoctor -r asciidoctor-bibtex -r asciidoctor-diagram -o Output/article.html article.adoc
asciidoctor-revealjs -r asciidoctor-bibtex -r asciidoctor-diagram -o Output/slides.html slides.adoc
----

////
vim: spell:spelllang=en_gb:syntax=asciidoc:textwidth=79:linespace=3:
////
