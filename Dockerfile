FROM asciidoctor/docker-asciidoctor

# Dev packages needed to install the Node.js package canvas, which is a
# dependency to install vega-cli.
RUN apk add --update --no-cache \
		npm \
		nodejs \
		g++ \
		build-base \
		cairo-dev \
		jpeg-dev \
		pango-dev \
		pangomm-dev \
		musl-dev \
		giflib-dev \
		pixman-dev \
		libjpeg-turbo-dev \
		freetype-dev && \
	npm config set unsafe-perm true && \
	npm install -g \
		vega \
		vega-lite \
		vega-cli && \
	gem install asciidoctor-bibtex && \
	apk del \
		build-base \
		g++ \
		cairo-dev \
		jpeg-dev \
		pango-dev \
		pangomm-dev \
		musl-dev \
		giflib-dev \
		pixman-dev \
		libjpeg-turbo-dev \
		freetype-dev \
